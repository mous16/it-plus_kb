# Italian Windows keyboard layout with additional symbols

Italian Plus layout is a Windows keyboard layout with additional symbols, aimed to help developers and people that desire to use a more inclusive language while typing.

## Additional symbols

This layout is build on default Windows Italian layout, with the addition of the following symbols:
- `~` (tilde) on `AltGr + ì`
- `` ` `` (backtick) on `AltGr + '`
- `ə` (schwa) on `AltGr + o`
- `Ə` (capital schwa) on `AltGr + Shift + o`
- `ɜ` (open-mid central unrounded vowel) on `AltGr + i`
- `Ɜ` (capital open-mid central unrounded vowel) on `AltGr + Shift + i`

## Install

Precompiled packages are available on the release page, you can find the latest one [here](https://gitlab.com/mous16/it-plus_kb/-/releases/permalink/latest).   
Packages are not signed, you have to trust me or to build those from source.

